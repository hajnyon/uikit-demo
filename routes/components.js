export class Components {
    title = 0;
    auAcc = null;
    auCntd = null;

    anim = null;

    attached() {

        // refresh of accordion
        this.int = setInterval(() => {
            this.title += 1;
        }, 5 * 1000);

        // event example
        UIkit.util.on(this.auAcc, 'show', function () {
            alert('shown');
        });

    }

    detached() {
        delete this.int;
    }

    start() {
        // method example
        UIkit.countdown(this.auCntd).start();
    }

    stop() {
        // method example
        UIkit.countdown(this.auCntd).stop();
    }

    test() {
        this.anim.classList.remove('uk-animation-fade');
        setTimeout(() => {
            this.anim.classList.add('uk-animation-fade');
        }, 2000);
    }

}