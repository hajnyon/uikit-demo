export class App {

  configureRouter(config, router) {
    config.title = 'UI-Virtualization',
      config.map([
        {
          route: ['', 'components'],
          name: 'components',
          moduleId: './routes/components.js',
          nav: 1,
          title: 'Grid'
        }
      ]);
    this.router = router;
  }
}
